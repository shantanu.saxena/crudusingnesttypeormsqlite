const Joi = require('joi');

export const updateCatSchema = Joi.object({
  name: Joi.string(),
  age: Joi.number(),
  breed: Joi.string(),
});
