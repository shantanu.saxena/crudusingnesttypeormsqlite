import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum ERole {
  admin = 'ADMIN',
  customer = 'CUSTOMER',
}

@Entity()
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ nullable: false })
  password: string;

  @Column({ type: 'text', nullable: false })
  role: ERole;
}
