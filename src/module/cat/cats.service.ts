// import { Injectable,  } from '@nestjs/common';
// import { CatEntity } from 'src/db/entities/cat.entity';
// import { Repository } from 'typeorm';
// import { InjectRepository} from '@nestjs/typeorm';
// import { UpdateResult, DeleteResult } from 'typeorm';

// @Injectable()
// export class CatService {
//   constructor(
//     @InjectRepository(CatEntity)
//     private catRepository: Repository<CatEntity>,
// ) { }
  
//   async  getCats(): Promise<CatEntity[]> {
//     return await this.catRepository.find();
// }

//   async  create(cat: CatEntity): Promise<CatEntity> {
//     return await this.catRepository.save(cat);
// }
  
//   async delete(id): Promise<DeleteResult> {
//     return await this.catRepository.delete(id);
// }
//   async update(cat: CatEntity): Promise<UpdateResult> {
//     return await this.catRepository.update(cat.id, cat);
// } 
// }

// // async  getCat(): Promise<CatEntity[]> {
// //   return await this.catRepository.find(CatEntity.);
// // }

//   // getCat(id: number): ICat | string{
//   //   for(const cat of CATS){
//   //     if(cat.id==id)
//   //     return cat;
//   //   }
//   //   return 'Not Found';
//   // }

//   // getWithCond():Array<ICat> | string{
//   //   const ans=CATS.filter((cat)=>+cat.age<8 && +cat.age>10);
//   //   if(ans.length!=0)
//   //    return ans;
//   //   return 'Not Found';
//   // }
//   // async create({name,age,breed}: {name:string; age:number; breed:string}){
   
//   //   const cat = new CatEntity();
//   //   cat.name=name;
//   //   cat.age=age;
//   //   cat.breed=breed;
//   //   await cat.save()
//   //   return cat.toJSON();
//   // }
import { Injectable } from '@nestjs/common';
import { CatEntity } from 'src/db/entities/cat.entity';
import { Between } from 'typeorm';

@Injectable()
export class CatService {
  async getCats(): Promise<CatEntity[]> {
    const cats = await CatEntity.find();
    return cats;
  }

  async insert(name: string, age: number, breed: string) {
    await CatEntity.insert({
      name,
      age,
      breed,
    });
    return 'Saved record successfully!';
  }

  async getCat(id: number) {
    const cat = await CatEntity.findOne({
      where: {
        id: id,
      },
    });
    return cat ? cat : null;
  }

  async getCatsWithinAgeRange(ageL: number, ageH: number) {
    const cats = await CatEntity.find({
      where: [{ age: Between(ageL, ageH) }],
    });

    return cats;
  }

  async update(
    id: number,
    { name, age, breed }: { name?: string; age?: number; breed?: string },
  ) {
    const cat = await this.getCat(id);
    if (cat) {
      if (name) cat.name = name;
      if (age) cat.age = age;
      if (breed) cat.breed = breed;
      await cat.save();
      return cat.id;
    }
    return null;
  }

  async delete(id: number) {
    await CatEntity.delete({ id });
  }
}
