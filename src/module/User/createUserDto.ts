import { ERole } from 'src/db/entities/user.entity';

export class createUserDto {
  name: string;
  email: string;
  password: string;
  role: ERole;
}
