// import { Body, Controller, Get, Param, Post,Put, Delete} from '@nestjs/common';
// import { CatEntity } from 'src/db/entities/cat.entity';

// import {CatService} from 'src/module/cats.service'

// @Controller('/cat' )
// export class CatController {
//   constructor(private readonly catService: CatService) {}

//   @Get('')
//   getCats(): Promise<CatEntity[]> {
//     return this.catService.getCats();
//     };

// //  @Get('/:id')
// //   getCat(@Param('id') id: number, @Body() catData: CatEntity):Promise<any>  {
// //     return this.catService.getCat(id);
// //     };
  
  
//   // @Get('WithCondition')
//   // getWithCond(): Promise<CatEntity[]>{
//   //   return this.catService.getWithCond();
//   // }

//   @Post('/post')
//   async create(@Body() catData: CatEntity): Promise<any>{
//    return this.catService.create(catData);
//   }

//   @Delete(':id/delete')
//     async delete(@Param('id') id): Promise<any> {
//       return this.catService.delete(id);
//     }

//    @Put(':id/update')
//     async update(@Param('id') id, @Body() catData: CatEntity): Promise<any> {
//         catData.id = Number(id);
//         console.log('Update #' + catData.id)
//         return this.catService.update(catData);
//     }  
// }
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
} from '@nestjs/common';
import { JoiValidationPipe } from 'src/JoiValidationPipe';
import { CatService } from './cats.service';
import { CreateCatDto } from './catDto';
import { createCatSchema } from '../../schema/catSchema/createCatSchema';
import { catIdSchema } from '../../schema/catSchema/catIdSchema';
import { catAgeRangeSchema } from 'src/schema/catSchema/catAgeRangeSchema';
import { updateCatSchema } from 'src/schema/catSchema/updateCatSchema';

@Controller()
export class CatController {
  constructor(private readonly catService: CatService) {}

  @Get()
  async getCats() {
    const cats = await this.catService.getCats();
    return cats.length ? cats : 'No cats found!';
  }

  @Get(':id')
  @UsePipes(new JoiValidationPipe(catIdSchema))
  async getCat(@Param() { id }: { id: number }) {
    const cat = await this.catService.getCat(id);
    if (!cat)
      throw new HttpException(
        'No record found with given id',
        HttpStatus.NOT_FOUND,
      );
    return cat;
  }

  @Get('withCondition')
  @UsePipes(new JoiValidationPipe(catAgeRangeSchema))
  async getCatsWithinAgeRange(
    @Query() { age_lte, age_gte }: { age_lte: number; age_gte: number },
  ) {
    const cats = await this.catService.getCatsWithinAgeRange(age_lte, age_gte);
    return cats.length ? cats : 'Not found!';
  }

  
  @Put(':id')
  async update(
    @Param(new JoiValidationPipe(catIdSchema)) { id }: { id: number },
    @Body(new JoiValidationPipe(updateCatSchema))
    updateCatDto: CreateCatDto,
  ) {
    const catID = await this.catService.update(id, updateCatDto);
    if (!catID)
      throw new HttpException(
        'No record for the specified ID',
        HttpStatus.NOT_FOUND,
      );
    return 'Record updated successfully!';
  }

  @Post()
  @UsePipes(new JoiValidationPipe(createCatSchema))
  async insert(@Body() createCatDto: CreateCatDto): Promise<string> {
    return await this.catService.insert(
      createCatDto.name,
      createCatDto.age,
      createCatDto.breed,
    );
  }

  @Delete(':id')
  @UsePipes(new JoiValidationPipe(catIdSchema))
  async delete(@Param() { id }: { id: number }) {
    return this.catService.delete(id);
  }
}
